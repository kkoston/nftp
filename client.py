from proto import Proto
import socket
import re
import os
import time

class Connection():
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.s.connect(('zetor.koston.us', 443))
        self.p = Proto(self.s)
        
        self.actions = {
            "ls": self.print_resp,
            "dir": self.print_resp,
            "cd": self.print_resp,
            "get": self.get            
        }          

    def print_resp(self, buff):
        self.send(buff)
        print self.recv()
        
    def get(self, buff):
        self.send(buff)
        resp = self.recv()
        try:
            size = int(resp)                
        except ValueError:
            print resp
            return
        
        dst_name = os.path.basename(re.split('\s+', buff)[1])
        
        f = open(dst_name + '.text', 'w+')
        br = 0
        
        stime = time.time()
        while br < size:
            rbuff = self.s.recv(65535)
            br += len(rbuff)
            f.write(rbuff)
            if et
                etime = time.time()
                print "%.2f bytes/sec" % (float(br) / (etime - stime), )
                    
        f.close()

        
        self.send("Transfer complete")
            
            

    def send(self, buff):
        self.p.send(buff)
    
    def recv(self):
        return self.p.recv()

    def read_cmd(self, prompt):
        success = False
        while not success:
            buff = raw_input(prompt).strip()
            cmd = re.split('\s+', buff)
        
            if len(cmd) == 0:
                continue
            
            if not cmd[0] in self.actions:
                print 'Unknown command'
                continue
            
            success = True
        
        return cmd[0], buff
    
    def main(self):
        while True:
            prompt = self.recv()
            cmd, buff = self.read_cmd(prompt)            
            self.actions[cmd](buff)
            
c = Connection()
c.main()
