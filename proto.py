import msgpack
from socket import MSG_PEEK
import struct

class Proto():
    FMT = '<l'
    BUFSZ = 1024
    
    def __init__(self, s):
        self.sock = s
            
    def send(self, buff):
        buff = msgpack.packb(buff)
        packet_size = len(buff)
        size_struct = struct.pack(self.FMT, packet_size)
        buff = size_struct + buff
        
        buff_len = len(buff)
        total = 0
        while total < buff_len:
            bs = self.sock.send(buff[total:])
            if bs == 0:
                return 0
            total = total + bs 
        return total  
        
    def recv(self):
        total = 0
        buff = ''
        br = 0
        
        # keep inspecting the recv buffer until at least 4 bytes received
        while (br < 4):
            buff = self.sock.recv(self.BUFSZ, MSG_PEEK)
            if len(buff) == 0:
                return ''
            br = len(buff)
        
        # receive 4 bytes only
        buff = self.sock.recv(4);
        packet_size = struct.unpack(self.FMT, buff)[0]
        
        # receive the packet (excat length)
        buff = ''
        while total < packet_size:
            tmp_buff = self.sock.recv(packet_size - total)
            if tmp_buff == '':
                return ''            
            total += len(tmp_buff)
            buff += tmp_buff
                            
        return msgpack.unpackb(buff)          
        