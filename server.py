from proto import Proto
import socket
import re
import os
import sys
import stat
from threading import Thread

class Connection(Thread):
    def __init__(self, afd):
        Thread.__init__(self)
        self.afd = afd
        self.afd.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.p = Proto(afd)
        
        self.actions = {
            "ls": self.ls,
            "dir": self.ls,
            "cd": self.cd,
            "get": self.get            
        }        
        
        self.wait()

    def ls(self, params):
        if len(params) > 0:
            d = params[0]
        else:
            d = os.getcwd()
            
        if not os.path.exists(d):
            self.send('Directory does not exist')
            return
        
        files = os.listdir(d)        
        resp = ''
        for f in files:
            df = d + '/' + f
            if os.path.isfile(df):
                ftype = 'f'
            elif os.path.isdir(df):
                ftype = 'd'
            else:
                ftype = 'u'
                
            resp += ftype + ' ' + f + '\n'
        
        self.send(resp)
        
    def cd(self, params):
        if len(params) > 0:
            d = params[0]
        else:
            self.send('Please provide directory to change to')
            return

        if not os.path.exists(d):
            self.send('Directory does not exist')
            return
        
        os.chdir(d)
        self.send("Directory changed to " + d)
        
    def get(self, params):
        if len(params) > 0:
            f = params[0]
        else:
            self.send('Please provide file to download')
            return

        if not os.path.exists(f):
            self.send('File does not exist')
            return
            
        st = os.stat(f)
        self.send(str(st.st_size))
        
        bs = 0
        fh = open(f, 'r')
        while bs < st.st_size:
            sbuff = fh.read(65535)
            self.afd.sendall(sbuff)
            bs += len(sbuff)
        fh.close()
        
        print self.recv()
    
    def send(self, buff):
        self.p.send(buff)
    
    def recv(self):
        return self.p.recv()
        
    def wait(self):
        while True:
            print "Sending prompt"
            self.send(os.getcwd() + ' > ')
            buff = self.recv()
            print 'Received: %d' % len(buff)
            cmd = re.split('\s+', buff)
            
            if len(cmd) == 0:
                continue
                
            if not cmd[0] in self.actions:
                self.send('Unknown command')
                continue
            
            self.actions[cmd[0]](cmd[1:])
            
    
    
        
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 10000))
s.listen(10)

while True:
    afd, addr = s.accept()    
    print "Client connected"    
    c = Connection(afd)
    c.setDaemon(True)
    c.start()